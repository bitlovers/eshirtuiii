﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShirt.Model.UI
{
    public class MenuItem:EShirtEntity
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public virtual Menu Menu { get; set; }
        public virtual MenuItem ParentMenuItem { get; set; }
        public string Url { get; set; }
        public int Level {
            get
            {
                if (this.ParentMenuItem == null)
                    return 0;
                else
                    return this.ParentMenuItem.Level + 1;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShirt.Model.UI
{
    public class Menu:EShirtEntity
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public List<MenuItem> MenuItems { get; set; }
    }
}

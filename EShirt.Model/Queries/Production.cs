﻿using EShirt.Model.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace EShirt.Model.Queries
{
    public static class Production
    {
        public static List<Category> GetAllCategories(EShirtContext _context)
        {
            return _context.Categories.ToList();
        }
        public static List<Category> GetAllRootCategories(EShirtContext _context)
        {
            List<Category> Categories = new List<Category>();
            foreach (Category item in _context.Categories)
            {
                if (item.Level == 0)
                {
                    Categories.Add(item);
                }
            }
            return Categories;
            // return _context.Categories.Where(c => c.Level == 0).ToList();

        }
        public static List<Category> GetSubCategoriesByParentCategoryID(EShirtContext _context, int _parentCategoryID)
        {
            return _context.Categories.Where(c => c.ParentCategory.ID == _parentCategoryID).ToList();
        }
        public static void AddRootCategoryByName(EShirtContext _context, string name)
        {
            _context.Categories.Add(new Category() { Name = name });
            _context.SaveChanges();
        }
        public static void AddCategoryByNameAndRootCategoryId(EShirtContext _context, string name, int id)
        {
            Category Item = new Category();
            Item.ParentCategory = _context.Categories.Where(c => c.ID == id).First();
            Item.Name = name;
            _context.Categories.Add(Item);
            _context.SaveChanges();
        }

        public static List<TreeNode> GetAllCategoriesTreeViewSource(EShirtContext _context)
        {
            List<TreeNode> source = new List<TreeNode>();
            List<Category> allCats = GetAllCategories(_context);

            foreach (var cat in allCats)
            {
                if (cat.ParentCategory == null)
                {
                    TreeNode treeRoot = new TreeNode();
                    treeRoot.Text = cat.Name;
                    treeRoot.Value = cat.ID.ToString();
                    treeRoot.ExpandAll();
                    source.Add(treeRoot);
                    generateCategoryChildNodes(_context, cat.ID, treeRoot);

                }
            }
            return source;
        }
        public static void DeleteCategory(EShirtContext _context, int id)
        {
            Category Item = _context.Categories.Find(id);
            _context.Entry(Item).State = System.Data.Entity.EntityState.Deleted;
            _context.SaveChanges();
        }
        public static void EditCategory(EShirtContext _context, int id, string name, int parentCategoryId)
        {
            Category Item = _context.Categories.Find(id);

            Item.Name = name;
            //Change.ParentCategory.ID = parentCategoryId;
            Item.ParentCategory = _context.Categories.SingleOrDefault(c => c.ID == parentCategoryId); //_context.Categories.Where(c => c.ID == parentCategoryId).First();
            _context.Entry(Item).State = System.Data.Entity.EntityState.Modified;
            _context.SaveChanges();
        }
        public static void AddProduct(EShirtContext _context, string name, int categoryId, int price, string description, int rate = 0)
        {
            Product Item = new Product();
            Category cat=_context.Categories.SingleOrDefault(c => c.ID == categoryId);
            if(cat!=null)
            {
                Item.Category = cat;
                Item.Name = name;
                Item.Price = price;
                Item.Description = description;
                Item.Rate = rate;
                //_context.Entry(Item).State = System.Data.Entity.EntityState.Added;
                _context.Products.Add(Item);
                _context.SaveChanges();
            }
       
            

        }
        private static void generateCategoryChildNodes(EShirtContext _context, int _parentCategoryID, TreeNode _treeRoot)
        {
            var childCats = GetSubCategoriesByParentCategoryID(_context, _parentCategoryID);
            foreach (var cat in childCats)
            {
                TreeNode node = new TreeNode();
                node.Text = cat.Name;
                node.Value = cat.ID.ToString();
                _treeRoot.ChildNodes.Add(node);
                generateCategoryChildNodes(_context, cat.ID, node);
            }
        }
    }
}

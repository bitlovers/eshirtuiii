﻿using EShirt.Model.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShirt.Model.Queries
{
    public static class UI
    {
        public static List<MenuItem> GetRootMenuItems(EShirtContext _context)
        {
            return _context.MenuItems.Where(mi => mi.Level == 0).ToList();
        }
        public static List<MenuItem> GetSubMenuItemsByParentMenuItemID(EShirtContext _context, int _parentMenuItemID)
        {
            return _context.MenuItems.Where(mi => mi.ParentMenuItem.ID == _parentMenuItemID).ToList();
        }
    }
}

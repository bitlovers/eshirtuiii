namespace EShirt.Model.Migrations
{
    using EShirt.Model.Production;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<EShirt.Model.EShirtContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(EShirt.Model.EShirtContext context)
        {
            var men = new Category { Name = "Men" };
            var women = new Category { Name = "Women" };
            
            context.Categories.AddOrUpdate(
              p => p.Name,
              men,
              women
            );


            context.Categories.AddOrUpdate(
              p => p.Name,
              new Category { Name = "Shoes" , ParentCategory = men},
              new Category { Name = "Women Shoes", ParentCategory = women},
              new Category { Name = "Bikes" , ParentCategory= men}
            );
            
        }
    }
}

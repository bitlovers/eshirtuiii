namespace EShirt.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UIMenuEntitiesAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "UI.MenuItem",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Menu_ID = c.Int(),
                        ParentMenuItem_ID = c.Int(),
                        CreatedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Name = c.String(),
                        Title = c.String(),
                        Url = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("UI.Menu", t => t.Menu_ID)
                .ForeignKey("UI.MenuItem", t => t.ParentMenuItem_ID)
                .Index(t => t.Menu_ID)
                .Index(t => t.ParentMenuItem_ID);
            
            CreateTable(
                "UI.Menu",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CreatedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Name = c.String(),
                        Title = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("UI.MenuItem", "ParentMenuItem_ID", "UI.MenuItem");
            DropForeignKey("UI.MenuItem", "Menu_ID", "UI.Menu");
            DropIndex("UI.MenuItem", new[] { "ParentMenuItem_ID" });
            DropIndex("UI.MenuItem", new[] { "Menu_ID" });
            DropTable("UI.Menu");
            DropTable("UI.MenuItem");
        }
    }
}

namespace EShirt.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirstMigrationProductionSchemaAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Production.ProductDetail_Image",
                c => new
                    {
                        ImageID = c.Int(nullable: false),
                        ProductDetailID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ImageID, t.ProductDetailID })
                .ForeignKey("Production.ProductDetail", t => t.ImageID)
                .ForeignKey("Production.Image", t => t.ProductDetailID)
                .Index(t => t.ImageID)
                .Index(t => t.ProductDetailID);
            
            CreateTable(
                "Production.Product_Tag",
                c => new
                    {
                        TagID = c.Int(nullable: false),
                        ProductID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.TagID, t.ProductID })
                .ForeignKey("Production.Product", t => t.TagID)
                .ForeignKey("Production.Tag", t => t.ProductID)
                .Index(t => t.TagID)
                .Index(t => t.ProductID);
            
            CreateTable(
                "Production.Category",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Image_ID = c.Int(),
                        ParentCategory_ID = c.Int(),
                        CreatedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Name = c.String(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("Production.Image", t => t.Image_ID)
                .ForeignKey("Production.Category", t => t.ParentCategory_ID)
                .Index(t => t.Image_ID)
                .Index(t => t.ParentCategory_ID);
            
            CreateTable(
                "Production.Image",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CreatedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Path = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "Production.ProductDetail",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Product_ID = c.Int(nullable: false),
                        ProductDetailType_ID = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Value = c.String(),
                        PriceDifference = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("Production.Product", t => t.Product_ID)
                .ForeignKey("Production.ProductDetailType", t => t.ProductDetailType_ID)
                .Index(t => t.Product_ID)
                .Index(t => t.ProductDetailType_ID);
            
            CreateTable(
                "Production.ProductDetailType",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CreatedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Name = c.String(),
                        DataType = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "Production.Product",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Category_ID = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Name = c.String(nullable: false),
                        Price = c.Int(nullable: false),
                        Description = c.String(),
                        Rate = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("Production.Category", t => t.Category_ID)
                .Index(t => t.Category_ID);
            
            CreateTable(
                "Production.Tag",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CreatedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Name = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.ID)
                .Index(t => t.Name, unique: true);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Production.Product", "Category_ID", "Production.Category");
            DropForeignKey("Production.ProductDetail", "ProductDetailType_ID", "Production.ProductDetailType");
            DropForeignKey("Production.ProductDetail", "Product_ID", "Production.Product");
            DropForeignKey("Production.Category", "ParentCategory_ID", "Production.Category");
            DropForeignKey("Production.Category", "Image_ID", "Production.Image");
            DropForeignKey("Production.Product_Tag", "ProductID", "Production.Tag");
            DropForeignKey("Production.Product_Tag", "TagID", "Production.Product");
            DropForeignKey("Production.ProductDetail_Image", "ProductDetailID", "Production.Image");
            DropForeignKey("Production.ProductDetail_Image", "ImageID", "Production.ProductDetail");
            DropIndex("Production.Tag", new[] { "Name" });
            DropIndex("Production.Product", new[] { "Category_ID" });
            DropIndex("Production.ProductDetail", new[] { "ProductDetailType_ID" });
            DropIndex("Production.ProductDetail", new[] { "Product_ID" });
            DropIndex("Production.Category", new[] { "ParentCategory_ID" });
            DropIndex("Production.Category", new[] { "Image_ID" });
            DropIndex("Production.Product_Tag", new[] { "ProductID" });
            DropIndex("Production.Product_Tag", new[] { "TagID" });
            DropIndex("Production.ProductDetail_Image", new[] { "ProductDetailID" });
            DropIndex("Production.ProductDetail_Image", new[] { "ImageID" });
            DropTable("Production.Tag");
            DropTable("Production.Product");
            DropTable("Production.ProductDetailType");
            DropTable("Production.ProductDetail");
            DropTable("Production.Image");
            DropTable("Production.Category");
            DropTable("Production.Product_Tag");
            DropTable("Production.ProductDetail_Image");
        }
    }
}

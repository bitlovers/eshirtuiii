namespace EShirt.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CombinationAdded : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Production.ProductDetail_Image", "ImageID", "Production.ProductDetail");
            DropForeignKey("Production.ProductDetail_Image", "ProductDetailID", "Production.Image");
            DropForeignKey("Production.ProductDetail", "Product_ID", "Production.Product");
            DropIndex("Production.ProductDetail_Image", new[] { "ImageID" });
            DropIndex("Production.ProductDetail_Image", new[] { "ProductDetailID" });
            DropIndex("Production.ProductDetail", new[] { "Product_ID" });
            CreateTable(
                "Production.Combination_Image",
                c => new
                    {
                        ImageID = c.Int(nullable: false),
                        CombinationID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ImageID, t.CombinationID })
                .ForeignKey("Production.Combination", t => t.ImageID)
                .ForeignKey("Production.Image", t => t.CombinationID)
                .Index(t => t.ImageID)
                .Index(t => t.CombinationID);
            
            CreateTable(
                "Production.Combination",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Product_ID = c.Int(),
                        CreatedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Name = c.String(),
                        PriceDifference = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("Production.Product", t => t.Product_ID)
                .Index(t => t.Product_ID);
            
            AddColumn("Production.ProductDetail", "Combination_ID", c => c.Int());
            CreateIndex("Production.ProductDetail", "Combination_ID");
            AddForeignKey("Production.ProductDetail", "Combination_ID", "Production.Combination", "ID");
            DropColumn("Production.ProductDetail", "Product_ID");
            DropColumn("Production.ProductDetail", "PriceDifference");
            DropColumn("Production.ProductDetail", "Quantity");
            DropTable("Production.ProductDetail_Image");
        }
        
        public override void Down()
        {
            CreateTable(
                "Production.ProductDetail_Image",
                c => new
                    {
                        ImageID = c.Int(nullable: false),
                        ProductDetailID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ImageID, t.ProductDetailID });
            
            AddColumn("Production.ProductDetail", "Quantity", c => c.Int(nullable: false));
            AddColumn("Production.ProductDetail", "PriceDifference", c => c.Int(nullable: false));
            AddColumn("Production.ProductDetail", "Product_ID", c => c.Int(nullable: false));
            DropForeignKey("Production.ProductDetail", "Combination_ID", "Production.Combination");
            DropForeignKey("Production.Combination", "Product_ID", "Production.Product");
            DropForeignKey("Production.Combination_Image", "CombinationID", "Production.Image");
            DropForeignKey("Production.Combination_Image", "ImageID", "Production.Combination");
            DropIndex("Production.ProductDetail", new[] { "Combination_ID" });
            DropIndex("Production.Combination", new[] { "Product_ID" });
            DropIndex("Production.Combination_Image", new[] { "CombinationID" });
            DropIndex("Production.Combination_Image", new[] { "ImageID" });
            DropColumn("Production.ProductDetail", "Combination_ID");
            DropTable("Production.Combination");
            DropTable("Production.Combination_Image");
            CreateIndex("Production.ProductDetail", "Product_ID");
            CreateIndex("Production.ProductDetail_Image", "ProductDetailID");
            CreateIndex("Production.ProductDetail_Image", "ImageID");
            AddForeignKey("Production.ProductDetail", "Product_ID", "Production.Product", "ID");
            AddForeignKey("Production.ProductDetail_Image", "ProductDetailID", "Production.Image", "ID");
            AddForeignKey("Production.ProductDetail_Image", "ImageID", "Production.ProductDetail", "ID");
        }
    }
}

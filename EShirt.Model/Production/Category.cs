﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace EShirt.Model.Production
{
    public class Category:EShirtEntity
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        [Column("ImageID")]
        public virtual Image Image { get; set; }
        [Column("ParentCategoryID")]
        public  virtual Category ParentCategory { get; set; }

        [Required]
        public int Level {

            get {
                if (ParentCategory == null)
                    return 0;
                else
                    return ParentCategory.Level + 1;
            }
        
        }
        public virtual List<Product> Products { get; set; }
    }
}

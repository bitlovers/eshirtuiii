﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace EShirt.Model.Production
{
    public class Product:EShirtEntity
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public int Price { get; set; }
        public string Description { get; set; }
        public int Rate { get; set; }
        [Required, Column("CategoryID")]
        public virtual Category Category { get; set; }
        public virtual List<Tag> Tags { get; set; }
        public virtual List<Combination> Combinations { get; set; }
    }
}

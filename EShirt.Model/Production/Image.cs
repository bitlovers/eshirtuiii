﻿using EShirt.Model.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EShirt.Model.Production
{
    public class Image:EShirtEntity
    {
        public string Path { get; set; }
        public virtual List<Category> Categories { get; set; }
        public virtual List<Combination> Combinations { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EShirt.Model.Production
{
    public class ProductDetailType:EShirtEntity
    {
        public string Name { get; set; }
        public string DataType { get; set; }
    }
}

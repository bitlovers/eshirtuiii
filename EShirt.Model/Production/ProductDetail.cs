﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace EShirt.Model.Production
{
    public class ProductDetail:EShirtEntity
    {
        [Required, Column("ProductDetailTypeID")]
        public virtual ProductDetailType ProductDetailType { get; set; }
        public Combination Combination { get; set; }
        public string Value { get; set; }
    }
}

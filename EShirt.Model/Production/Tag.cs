﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace EShirt.Model.Production
{
    public class Tag:EShirtEntity
    {
        [Required, Index(IsUnique=true, IsClustered=false),StringLength(50)]
        public string Name { get; set; }
        public virtual List<Product> Products { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShirt.Model.Production
{
    public class Combination:EShirtEntity
    {
        public string Name { get; set; }
        /// <summary>
        /// Use + or - Values in Price Diff to Original Price defined in Product Entity
        /// </summary>
        public int PriceDifference { get; set; }
        public int Quantity { get; set; }
        public virtual List<Image> Images { get; set; }
        public virtual List<ProductDetail> ProductDetails { get; set; }
        public virtual Product Product { get; set; }

    }
}

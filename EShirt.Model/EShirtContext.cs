﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EShirt.Model.Production;
using EShirt.Model.UI;

namespace EShirt.Model
{
    public class EShirtContext:DbContext
    {
        public DbSet<EShirtEntity> EShirtEntities { get; set; }

        //Production
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductDetail> ProductDetails { get; set; }
        public DbSet<ProductDetailType> ProductDetailTypes { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<Combination> Combinations { get; set; }


        //UI
        public DbSet<Menu> Menus { get; set; }
        public DbSet<MenuItem> MenuItems { get; set; }


        public EShirtContext()
            :base("server=.;database=Eshirt.Dev;integrated security=sspi;")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Add(new DateTime2Convention());

            modelBuilder.Entity<Product>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Product", schemaName: "Production");
            })
            .HasMany(p => p.Tags)
            .WithMany(t => t.Products)
            .Map(m =>
            {
                m.MapLeftKey("TagID");
                m.MapRightKey("ProductID");
                m.ToTable("Product_Tag", schemaName: "Production");
            });


            modelBuilder.Entity<ProductDetail>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("ProductDetail", schemaName: "Production");
            });
            modelBuilder.Entity<Combination>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Combination", schemaName: "Production");
            })
            .HasMany(c => c.Images)
            .WithMany(i => i.Combinations)
            .Map(m =>
            {
                m.MapLeftKey("ImageID");
                m.MapRightKey("CombinationID");
                m.ToTable("Combination_Image", schemaName: "Production");
            });
            modelBuilder.Entity<ProductDetailType>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("ProductDetailType", schemaName: "Production");
            });
            modelBuilder.Entity<Category>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Category", schemaName: "Production");
            });
            modelBuilder.Entity<Tag>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Tag", schemaName: "Production");
            });
            modelBuilder.Entity<Image>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Image", schemaName: "Production");
            });
            modelBuilder.Entity<Menu>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Menu", schemaName: "UI");
            });
            modelBuilder.Entity<MenuItem>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("MenuItem", schemaName: "UI");
            });


            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            var entities = ChangeTracker.Entries()
                .Where(x => x.Entity is EShirtEntity && (x.State == EntityState.Added || x.State == EntityState.Modified));

            foreach (var entity in entities)
            {
                if (entity.State == EntityState.Added)
                {
                    ((EShirtEntity)entity.Entity).CreatedDate = DateTime.Now;
                }

                ((EShirtEntity)entity.Entity).ModifiedDate = DateTime.Now;
            }

            return base.SaveChanges();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EshirtUI.Models.Api
{
    public class FilterProduct
    {
        public string dtl1 { get; set; }
        public string dtl2 { get; set; }
        public string dtl3 { get; set; }
        public string dtl4 { get; set; }
        public string href { get; set; }
        public List<string> images { get; set; }
        public FilterProduct()
        {
            images = new List<string>();
        }
        public FilterProduct(string _Name, string _Price, string _Category,string _Colors,string _Href, List<string> _Images)
        {
            dtl2 = _Name;
            dtl4 = _Price;
            dtl3 = _Category;
            dtl1 = _Colors;
            href = _Href;
            images = _Images;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EshirtUI.Models.Api
{
    public enum FilterValueTypes
    {
        OnlyText = 1,
        WithImages = 2
    }
    public class FilterMenu
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public int? ParentId { get; set; }
        public int? SortIndex { get; set; }
        public FilterValueTypes Type { get; set; }
        public List<FilterValue> Values { get; set; }
        public FilterMenu()
        {
            Values = new List<FilterValue>();
        }
        public FilterMenu(int _Id, string _Name, string _Text, int? _ParentId, int? _SortIndex, FilterValueTypes _Type)
            : this()
        {
            Id = _Id;
            Name = _Name;
            Text = _Text;
            SortIndex = _SortIndex;
            Type = _Type;
            ParentId = _ParentId;

        }
        public FilterMenu(int _Id, string _Name, string _Text, int? _ParentId, int? _SortIndex, FilterValueTypes _Type, List<FilterValue> _Values)
        {
            Id = _Id;
            Name = _Name;
            Text = _Text;
            SortIndex = _SortIndex;
            Type = _Type;
            Values = _Values;
            ParentId = _ParentId;
        }
    }
}
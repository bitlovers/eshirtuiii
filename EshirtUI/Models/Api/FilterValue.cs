﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EshirtUI.Models.Api
{
    public class FilterValue
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public string BackImgUrl { get; set; }
        public FilterValue()
        {

        }
        public FilterValue(string _Text, string _Value)
        {
            Text = _Text;
            Value = _Value;
        }
        public FilterValue(string _Text, string _Value, string _BackImgUrl)
        {
            Text = _Text;
            Value = _Value;
            BackImgUrl = _BackImgUrl;
        }
    }
}
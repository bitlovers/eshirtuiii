﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EshirtUI.Models.Api;
using System.Threading;

namespace EshirtUI.Controllers.Api
{
    public class FiltersController : ApiController
    {
        [HttpPost]
        public string GetFilters()
        {
            //Get this list from database
            List<FilterMenu> Filters = new List<FilterMenu>() 
            { 
                new FilterMenu(1 , "Gender" , "جنسیت" , null , 1 , FilterValueTypes.OnlyText , new List<FilterValue>()
                {
                    new FilterValue("زنانه","Womens"),
                    new FilterValue("مردانه","Mens"),
                    new FilterValue("دخترانه","Girls"),
                    new FilterValue("پسرانه","Boys")
                }),
                new FilterMenu(2 , "Category1" , "گروه" , null , 2 , FilterValueTypes.OnlyText , new List<FilterValue>()
                {
                    new FilterValue("بالا تنه","Tops"),
                    new FilterValue("پایین تنه","Bottoms"),
                    new FilterValue("کفش","Shoes"),
                    new FilterValue("اکسسوری","Accessories")
                }),
                 new FilterMenu(3 , "Category2","نوع" , 2 , 3 , FilterValueTypes.OnlyText , new List<FilterValue>()
                {
                    new FilterValue("بالا تنه","Tops"),
                    new FilterValue("پایین تنه","Bottoms"),
                    new FilterValue("کفش","Shoes"),
                    new FilterValue("اکسسوری","Accessories")
                }),
                new FilterMenu(4 , "Color","رنگ" , null , 4 , FilterValueTypes.WithImages , new List<FilterValue>()
                {
                    new FilterValue("زرد","Yellow","http://ex.com/images/yellow.jpg"),
                    new FilterValue("قرمز","Red","http://ex.com/images/Red.jpg"),
                    new FilterValue("صورتی","Pink","http://ex.com/images/Pink.jpg"),
                    new FilterValue("مشکی","Black","http://ex.com/images/Black.jpg")
                }),
            };
            Thread.Sleep(1000);
            StatusCode(HttpStatusCode.OK);
            return Newtonsoft.Json.JsonConvert.SerializeObject(Filters);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EshirtUI.Models.Api;
using System.Threading;
using EShirt.Model;
namespace EshirtUI.Controllers.Api
{
    public class ProductsController : ApiController
    {

        public string Index()
        {
            Thread.Sleep(3000);
            return Newtonsoft.Json.JsonConvert.SerializeObject(SampleProducts);
        }




        [AttributeRouting.Web.Mvc.GET("Api/Products/FilterProducts/{FiltersJson}")]
        public string FilterProducts(string FiltersJson)
        {
            List<FilterItem> requestedFilters = Newtonsoft.Json.JsonConvert.DeserializeObject<List<FilterItem>>(FiltersJson);
            List<FilterProduct> output = new List<FilterProduct>();

            // get all products
            EShirtContext c = new EShirtContext();
            IQueryable<EShirt.Model.Production.Product> Products = c.Products;

            //Query by filters received 
            foreach (FilterItem filter in requestedFilters)
            {
                switch (filter.name)
                {
                    case "Gender":
                        //Products = Products.Where(p => p.# What # == Convert.ToInt32(filter.value));
                        break;
                    case "Category1":
                        Products = Products.Where(p => p.Category.ID == Convert.ToInt32(filter.value));
                        break;
                    case "Category2":
                        //Products = Products.Where(p => p.# What # == Convert.ToInt32(filter.value));
                        break;
                    case "Color":
                        //Products = Products.Where(p => p.# What # == Convert.ToInt32(filter.value));
                        break;
                    default:
                        break;
                }
            }

            foreach (EShirt.Model.Production.Product p in Products)
            {
                output.Add(new FilterProduct(p.Name, string.Format("{0} تومان", p.Price), p.Category.Name, "Colors Count Must sit here", "/product/1254", new List<string>() { /* Image URL's */}));
            }

            //As long as the filtering code get ready we retuen random values
            output = new List<FilterProduct>();
            //Random r = new Random();
            //for (int i = 0; i < 10; i++)
            //{
            //    output.Add(SampleProducts[r.Next(0, SampleProducts.Count - 1)]);
            //}
            output.Add(SampleProducts.First());
            Thread.Sleep(1000);

            //return output list 
            return Newtonsoft.Json.JsonConvert.SerializeObject(output);
        }

        public List<FilterProduct> SampleProducts
        {
            get
            {
                return new List<FilterProduct>() 
                {
                    new FilterProduct("Nike Flyknit Racer","750,000 تومان","کفش دو مردانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/1img1.jpg",
                        "/Assets/Images/products/1img2.jpg",
                        "/Assets/Images/products/1img3.jpg",
                        "/Assets/Images/products/1img4.jpg",
                        "/Assets/Images/products/1img5.jpg"
                    }),
                    new FilterProduct("Nike Free 3.0 Flyknit","875,000 تومان","کفش مردانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/2img1.jpg",
                        "/Assets/Images/products/2img2.jpg",
                        "/Assets/Images/products/2img3.jpg",
                        "/Assets/Images/products/2img4.jpg",
                        "/Assets/Images/products/2img5.jpg"
                    }),
                    new FilterProduct("Nike Free","925,000 تومان","کفش زنانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/3img1.jpg",
                        "/Assets/Images/products/3img2.jpg",
                        "/Assets/Images/products/3img3.jpg",
                        "/Assets/Images/products/3img4.jpg",
                        "/Assets/Images/products/3img5.jpg"
                    }),
                    new FilterProduct("Nike Flyknit Racer","750,000 تومان","کفش دو مردانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/1img1.jpg",
                        "/Assets/Images/products/1img2.jpg",
                        "/Assets/Images/products/1img3.jpg",
                        "/Assets/Images/products/1img4.jpg",
                        "/Assets/Images/products/1img5.jpg"
                    }),
                    new FilterProduct("Nike Free 3.0 Flyknit","875,000 تومان","کفش مردانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/2img1.jpg",
                        "/Assets/Images/products/2img2.jpg",
                        "/Assets/Images/products/2img3.jpg",
                        "/Assets/Images/products/2img4.jpg",
                        "/Assets/Images/products/2img5.jpg"
                    }),
                    new FilterProduct("Nike Free","925,000 تومان","کفش زنانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/3img1.jpg",
                        "/Assets/Images/products/3img2.jpg",
                        "/Assets/Images/products/3img3.jpg",
                        "/Assets/Images/products/3img4.jpg",
                        "/Assets/Images/products/3img5.jpg"
                    }),
                    new FilterProduct("Nike Flyknit Racer","750,000 تومان","کفش دو مردانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/1img1.jpg",
                        "/Assets/Images/products/1img2.jpg",
                        "/Assets/Images/products/1img3.jpg",
                        "/Assets/Images/products/1img4.jpg",
                        "/Assets/Images/products/1img5.jpg"
                    }),
                    new FilterProduct("Nike Free 3.0 Flyknit","875,000 تومان","کفش مردانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/2img1.jpg",
                        "/Assets/Images/products/2img2.jpg",
                        "/Assets/Images/products/2img3.jpg",
                        "/Assets/Images/products/2img4.jpg",
                        "/Assets/Images/products/2img5.jpg"
                    }),
                    new FilterProduct("Nike Free","925,000 تومان","کفش زنانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/3img1.jpg",
                        "/Assets/Images/products/3img2.jpg",
                        "/Assets/Images/products/3img3.jpg",
                        "/Assets/Images/products/3img4.jpg",
                        "/Assets/Images/products/3img5.jpg"
                    }),
                    new FilterProduct("Nike Flyknit Racer","750,000 تومان","کفش دو مردانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/1img1.jpg",
                        "/Assets/Images/products/1img2.jpg",
                        "/Assets/Images/products/1img3.jpg",
                        "/Assets/Images/products/1img4.jpg",
                        "/Assets/Images/products/1img5.jpg"
                    }),
                    new FilterProduct("Nike Free 3.0 Flyknit","875,000 تومان","کفش مردانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/2img1.jpg",
                        "/Assets/Images/products/2img2.jpg",
                        "/Assets/Images/products/2img3.jpg",
                        "/Assets/Images/products/2img4.jpg",
                        "/Assets/Images/products/2img5.jpg"
                    }),
                    new FilterProduct("Nike Free","925,000 تومان","کفش زنانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/3img1.jpg",
                        "/Assets/Images/products/3img2.jpg",
                        "/Assets/Images/products/3img3.jpg",
                        "/Assets/Images/products/3img4.jpg",
                        "/Assets/Images/products/3img5.jpg"
                    }),
                    new FilterProduct("Nike Flyknit Racer","750,000 تومان","کفش دو مردانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/1img1.jpg",
                        "/Assets/Images/products/1img2.jpg",
                        "/Assets/Images/products/1img3.jpg",
                        "/Assets/Images/products/1img4.jpg",
                        "/Assets/Images/products/1img5.jpg"
                    }),
                    new FilterProduct("Nike Free 3.0 Flyknit","875,000 تومان","کفش مردانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/2img1.jpg",
                        "/Assets/Images/products/2img2.jpg",
                        "/Assets/Images/products/2img3.jpg",
                        "/Assets/Images/products/2img4.jpg",
                        "/Assets/Images/products/2img5.jpg"
                    }),
                    new FilterProduct("Nike Free","925,000 تومان","کفش زنانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/3img1.jpg",
                        "/Assets/Images/products/3img2.jpg",
                        "/Assets/Images/products/3img3.jpg",
                        "/Assets/Images/products/3img4.jpg",
                        "/Assets/Images/products/3img5.jpg"
                    }),
                    new FilterProduct("Nike Flyknit Racer","750,000 تومان","کفش دو مردانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/1img1.jpg",
                        "/Assets/Images/products/1img2.jpg",
                        "/Assets/Images/products/1img3.jpg",
                        "/Assets/Images/products/1img4.jpg",
                        "/Assets/Images/products/1img5.jpg"
                    }),
                    new FilterProduct("Nike Free 3.0 Flyknit","875,000 تومان","کفش مردانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/2img1.jpg",
                        "/Assets/Images/products/2img2.jpg",
                        "/Assets/Images/products/2img3.jpg",
                        "/Assets/Images/products/2img4.jpg",
                        "/Assets/Images/products/2img5.jpg"
                    }),
                    new FilterProduct("Nike Free","925,000 تومان","کفش زنانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/3img1.jpg",
                        "/Assets/Images/products/3img2.jpg",
                        "/Assets/Images/products/3img3.jpg",
                        "/Assets/Images/products/3img4.jpg",
                        "/Assets/Images/products/3img5.jpg"
                    }),
                    new FilterProduct("Nike Flyknit Racer","750,000 تومان","کفش دو مردانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/1img1.jpg",
                        "/Assets/Images/products/1img2.jpg",
                        "/Assets/Images/products/1img3.jpg",
                        "/Assets/Images/products/1img4.jpg",
                        "/Assets/Images/products/1img5.jpg"
                    }),
                    new FilterProduct("Nike Free 3.0 Flyknit","875,000 تومان","کفش مردانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/2img1.jpg",
                        "/Assets/Images/products/2img2.jpg",
                        "/Assets/Images/products/2img3.jpg",
                        "/Assets/Images/products/2img4.jpg",
                        "/Assets/Images/products/2img5.jpg"
                    }),
                    new FilterProduct("Nike Free","925,000 تومان","کفش زنانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/3img1.jpg",
                        "/Assets/Images/products/3img2.jpg",
                        "/Assets/Images/products/3img3.jpg",
                        "/Assets/Images/products/3img4.jpg",
                        "/Assets/Images/products/3img5.jpg"
                    }),
                    new FilterProduct("Nike Flyknit Racer","750,000 تومان","کفش دو مردانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/1img1.jpg",
                        "/Assets/Images/products/1img2.jpg",
                        "/Assets/Images/products/1img3.jpg",
                        "/Assets/Images/products/1img4.jpg",
                        "/Assets/Images/products/1img5.jpg"
                    }),
                    new FilterProduct("Nike Free 3.0 Flyknit","875,000 تومان","کفش مردانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/2img1.jpg",
                        "/Assets/Images/products/2img2.jpg",
                        "/Assets/Images/products/2img3.jpg",
                        "/Assets/Images/products/2img4.jpg",
                        "/Assets/Images/products/2img5.jpg"
                    }),
                    new FilterProduct("Nike Free","925,000 تومان","کفش زنانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/3img1.jpg",
                        "/Assets/Images/products/3img2.jpg",
                        "/Assets/Images/products/3img3.jpg",
                        "/Assets/Images/products/3img4.jpg",
                        "/Assets/Images/products/3img5.jpg"
                    }),
                    new FilterProduct("Nike Flyknit Racer","750,000 تومان","کفش دو مردانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/1img1.jpg",
                        "/Assets/Images/products/1img2.jpg",
                        "/Assets/Images/products/1img3.jpg",
                        "/Assets/Images/products/1img4.jpg",
                        "/Assets/Images/products/1img5.jpg"
                    }),
                    new FilterProduct("Nike Free 3.0 Flyknit","875,000 تومان","کفش مردانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/2img1.jpg",
                        "/Assets/Images/products/2img2.jpg",
                        "/Assets/Images/products/2img3.jpg",
                        "/Assets/Images/products/2img4.jpg",
                        "/Assets/Images/products/2img5.jpg"
                    }),
                    new FilterProduct("Nike Free","925,000 تومان","کفش زنانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/3img1.jpg",
                        "/Assets/Images/products/3img2.jpg",
                        "/Assets/Images/products/3img3.jpg",
                        "/Assets/Images/products/3img4.jpg",
                        "/Assets/Images/products/3img5.jpg"
                    }),
                    new FilterProduct("Nike Flyknit Racer","750,000 تومان","کفش دو مردانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/1img1.jpg",
                        "/Assets/Images/products/1img2.jpg",
                        "/Assets/Images/products/1img3.jpg",
                        "/Assets/Images/products/1img4.jpg",
                        "/Assets/Images/products/1img5.jpg"
                    }),
                    new FilterProduct("Nike Free 3.0 Flyknit","875,000 تومان","کفش مردانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/2img1.jpg",
                        "/Assets/Images/products/2img2.jpg",
                        "/Assets/Images/products/2img3.jpg",
                        "/Assets/Images/products/2img4.jpg",
                        "/Assets/Images/products/2img5.jpg"
                    }),
                    new FilterProduct("Nike Free","925,000 تومان","کفش زنانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/3img1.jpg",
                        "/Assets/Images/products/3img2.jpg",
                        "/Assets/Images/products/3img3.jpg",
                        "/Assets/Images/products/3img4.jpg",
                        "/Assets/Images/products/3img5.jpg"
                    }),
                    new FilterProduct("Nike Flyknit Racer","750,000 تومان","کفش دو مردانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/1img1.jpg",
                        "/Assets/Images/products/1img2.jpg",
                        "/Assets/Images/products/1img3.jpg",
                        "/Assets/Images/products/1img4.jpg",
                        "/Assets/Images/products/1img5.jpg"
                    }),
                    new FilterProduct("Nike Free 3.0 Flyknit","875,000 تومان","کفش مردانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/2img1.jpg",
                        "/Assets/Images/products/2img2.jpg",
                        "/Assets/Images/products/2img3.jpg",
                        "/Assets/Images/products/2img4.jpg",
                        "/Assets/Images/products/2img5.jpg"
                    }),
                    new FilterProduct("Nike Free","925,000 تومان","کفش زنانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/3img1.jpg",
                        "/Assets/Images/products/3img2.jpg",
                        "/Assets/Images/products/3img3.jpg",
                        "/Assets/Images/products/3img4.jpg",
                        "/Assets/Images/products/3img5.jpg"
                    }),
                    new FilterProduct("Nike Flyknit Racer","750,000 تومان","کفش دو مردانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/1img1.jpg",
                        "/Assets/Images/products/1img2.jpg",
                        "/Assets/Images/products/1img3.jpg",
                        "/Assets/Images/products/1img4.jpg",
                        "/Assets/Images/products/1img5.jpg"
                    }),
                    new FilterProduct("Nike Free 3.0 Flyknit","875,000 تومان","کفش مردانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/2img1.jpg",
                        "/Assets/Images/products/2img2.jpg",
                        "/Assets/Images/products/2img3.jpg",
                        "/Assets/Images/products/2img4.jpg",
                        "/Assets/Images/products/2img5.jpg"
                    }),
                    new FilterProduct("Nike Free","925,000 تومان","کفش زنانه","5 رنگ","/product/1254",new List<string>()
                    {
                        "/Assets/Images/products/3img1.jpg",
                        "/Assets/Images/products/3img2.jpg",
                        "/Assets/Images/products/3img3.jpg",
                        "/Assets/Images/products/3img4.jpg",
                        "/Assets/Images/products/3img5.jpg"
                    }),
                    

                };
            }
        }
    }
}

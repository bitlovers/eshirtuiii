﻿
showScreenLoader=function(){
	$('.screenLoader').addClass('active');
}
hideScreenLoader =function(){
	$('.screenLoader').removeClass('active');
}
function pushHistoryState(_items){
	if(_items.length){
		var pushUrl=window.location.pathname+'?';
		for (var i = 0; i < _items.length; i++) {
			var item=_items[i];
			pushUrl+=item.name+'='+item.value;
			if(i!=_items.length-1) pushUrl+='&';
		};
		window.history.pushState(_items,document.title,pushUrl);
	}
}
function getQSVals(){
	var vars = [], hash;
    var q = document.URL.split('?')[1];
    if(q != undefined){
        q = q.split('&');
        for(var i = 0; i < q.length; i++){
            hash = q[i].split('=');
            //vars.push(hash[1]);
            //vars[hash[0]] = hash[1];
            vars[i]={name:hash[0],value:hash[1]};
        }
	}
	return vars;
}
function navController(){
	var _this=this;
	this.subnavs=[];
	this.subnavContainer=$('.subnav-container');
	this.isOpen=false;
	this.activenav=undefined;
	this.closing=false;
	this.opening=false;
	this.openQueue=undefined;
	this.navsearchclone=undefined;
	this.openNavSearch=function(){
		_this.navsearchclone=$('<div class="navsearch navsearchclone"></div>').append($('.navsearch form').clone().css({'display':'block'})).css({
			'top':($('.navsearch').offset().top+'px'),
			'left':($('.navsearch').offset().left+'px'),
			'width':($('.navsearch').width()),
			'max-width':'auto',
			'opacity':'0',
			'display':'block',
			'z-index':'2120',
			'position':'absolute',
			'background-image':"url(../images/navbg.jpg)"
		}).appendTo('div.wrapper').animate({opacity:1,left:5,width:($('.nav.bignav').outerWidth(true)-10)+'px'},300,'swing',function(){
			$('input',_this.navsearchclone).focus().blur(_this.closeNavSearch);
			$('input',_this.navsearchclone);
			$(window).resize(_this.closeNavSearch);
		});
		$(_this.navsearchclone).focus();
	};
	this.closeNavSearch=function (){
		if(typeof _this.navsearchclone!='undefined'){
			$(_this.navsearchclone).animate({width:0,opacity:0},300,'swing',function(){
				 _this.navsearchclone.remove();
				 _this.navsearchclone=undefined;
			});
		}
	};
	this.noOpenRequest=function(){
		for(var i=0; i< _this.subnavs.length; i++){
			if(_this.subnavs[i].openRequest) return false;
		}
		return true;
	};
	this.deactiveAll=function(){
		for(var i=0; i < _this.subnavs.length; i++){
			_this.subnavs[i].isActive=false;
			$(_this.subnavs[i].menuitem).removeClass('active');
		}
	};

	this.openRequest=function(sbnv){
		if(!sbnv.isActive){
			setTimeout(function(sbnv){
				if(sbnv.openRequest){
					_this.opening=true;
					if(!_this.isOpen){
						setTimeout(function(sbnv){
							if(sbnv.openRequest){
								if(!_this.closing){
									_this.deactiveAll();
									_this.isOpen=true;
									sbnv.isActive=true;
									sbnv.openRequest=false;
									$(sbnv.menuitem).addClass('active');
									$(sbnv.sbnvp).appendTo(_this.subnavContainer);
									if(sbnv.type=='login') $(_this.subnavContainer).addClass('sbnvclogin');
									else $(_this.subnavContainer).removeClass('sbnvclogin');
									$(sbnv.sbnvp).animate({opacity:1},300,'swing');
									sbnv.order();
									$(_this.subnavContainer).height($(sbnv.subnav).outerHeight(true));
									_this.activenav=sbnv;
								} else _this.openQueue = sbnv;
							}
						},250,sbnv);
					}
					else {
						setTimeout(function(sbnv){
							if(sbnv.openRequest){
								if(!_this.closing){
									_this.deactiveAll();
									$(_this.activenav.menuitem).removeClass('active');
									$(sbnv.sbnvp).appendTo(_this.subnavContainer);
									if(sbnv.type=='login') $(_this.subnavContainer).addClass('sbnvclogin');
									else $(_this.subnavContainer).removeClass('sbnvclogin');
									$(sbnv.menuitem).addClass('active');
									_this.isOpen=true;
									sbnv.isActive=true;
									sbnv.openRequest=false;
									var fosbnv=_this.activenav.sbnvp;
									$(sbnv.sbnvp).stop().animate({opacity:1},200,"swing");
									$(_this.activenav.sbnvp).stop().animate({opacity:0},200,"swing",function(){
										$(fosbnv).detach();
									})
									_this.activenav=sbnv;
									sbnv.order();
									$(_this.subnavContainer).height($(sbnv.subnav).outerHeight(true));
								} else _this.openQueue = sbnv;
							} 
						},200,sbnv);
					}
				}
			},50,sbnv);
		}
	};
	this.afterClose=function(){
		if(typeof _this.openQueue!='undefined'){
			var hold=_this.openQueue;
			_this.openQueue=undefined;
			_this.openRequest(hold);
		}
	};
	this.closeRequest=function(sbnv){
		_this.opening=false;
		if(_this.noOpenRequest()){
			setTimeout(function(sbnv){
				if(_this.opening==false && _this.noOpenRequest()){
					_this.closing=true;
					sbnv.isActive=false;
					_this.isOpen=false;
					$(_this.subnavContainer).height(0);
					$(sbnv.menuitem).removeClass('active');
					
					$(sbnv.sbnvp).stop().animate({opacity:0},400,"swing",function(){
							$(sbnv.sbnvp).detach();
							_this.closing=false
							_this.afterClose();
						});
				}
			},200,sbnv);
		} else{
			sbnv.isActive=false;
			$(sbnv.menuitem).removeClass('active');
		}
	};



	//init...
	$('.nav.bignav li.menu-item').each(function(a,b){
		if($('.subnavitem-container',b).length)
			_this.subnavs[a-1]=new subnav(b,$('.subnav-container'),_this);
	});
	$('#btnOpenNavSearch').click(_this.openNavSearch);
	

}

function subnav(argLi,argContainer,argController){
	var _this=this;
	this.menuitem=argLi;
	this.subnav=$('.subnavitem-container',argLi).clone();
	this.sbnvp=$('<div></div>').addClass('x').css('opacity','0');
	this.container=argContainer;
	this.controller=argController;
	this.mihover=false;
	this.sbnvhover=false;
	this.isActive=false;
	this.ordered=false;
	this.openRequest=false;
	this.type=($(argLi).hasClass('navlogin'))?'login':'menu';
	this.order=function(){
		if(!_this.ordered){
			var height=$(_this.subnav).height();
			$('.subnav-col',_this.subnav).each(function(c,d){
				var usedH = $('h4',d).height() + $('ul',d).height();
				$('li.subnav-viewmore',d).css('margin-top',(height-usedH)+'px');
			});
			_this.ordered=true;
		}
	};
	var checkHover=function(){
		if(_this.mihover) { _this.openRequest=true; _this.controller.openRequest(_this);}
		else if(!_this.mihover && !_this.sbnvhover){ _this.openRequest=false; _this.controller.closeRequest(_this);}
	};
	$(_this.menuitem).mouseenter(function(){
		_this.mihover=true;
		setTimeout(checkHover,50);
	});
	$(_this.menuitem).click(function(e){
		e.preventDefault();
		_this.mihover=true;
		setTimeout(checkHover,50);
	});
	$(_this.menuitem).dblclick(function(){
		//alert('hello');
	});
	$(_this.menuitem).mouseleave(function(){
		_this.mihover=false;
		setTimeout(checkHover,50);
	});
	$(_this.container).mouseenter(function(){
		_this.sbnvhover=true;
		setTimeout(checkHover,50);
	});
	$(_this.container).mouseleave(function(){
		_this.sbnvhover=false;
		setTimeout(checkHover,50);
	});
	$(_this.subnav).css('display','table');
	$(_this.sbnvp).append(_this.subnav);
}

















































































function filterController(_opts){
	var _this = this;
	this.opts={
		getFilters:{
			url:'/',
			data:{},
			method:'get'
		},
		getProducts:{
			url:'/',
			method:'get',
		},
		productsController:new productsController(),
		filterTemplate:undefined,
		container:$('<div></div>')
	};
	$.extend(_this.opts,_opts);
	this.productsController=_this.opts.productsController;
	this.filters=[];

	this.filtersJson=[];
	this.filterTemplate=$($(_this.opts.filterTemplate).clone());
	this.filterElem=$($(_this.filterTemplate).clone());

	this.filterMenuTemplate=$($('.pf-menu',_this.filterTemplate).clone());
	this.ready=_this.opts.ready;
	this.pushingHistory=false;
	this.checkActiveValuesFromHistory=function(_obj){
		$(_obj).each(function(ri,robj){
			$(_this.filters).each(function(filterIndex,filter){
				if(filter.name==obj.name){
					$(filter.values).each(function(valI,value){
						if(value.value==robj.value){
							filter.valueRequesActivation(value);
						}
					});
				}
			});
		});
	};
	var getProductsTry=0,getFiltersTry=0;
	this.getFilters=function(){
		setTimeout(showScreenLoader,100);
		$.ajax({
			url:_this.opts.getFilters.url,
			method:_this.opts.getFilters.method,
			success:function(data,stat,xhr){
					getFiltersTry=0;
					_this.filtersJson=JSON.parse(data);
					onFiltersRecive();
					setTimeout(hideScreenLoader,100);
			},
			fail:function(){getFiltersTry++; setTimeout(_this.getFilters,getFiltersTry*1000);}
		});
	};
	var getProductsTry=0;
	this.getProducts=function(_filtersJsonString){
		setTimeout(showScreenLoader,200);
		
		$.ajax({
			url:_this.opts.getProducts.url,
			method:_this.opts.getProducts.method,
			data:[],
			success:function(data,stat,xhr){
				getProductsTry=0;
				_this.productsController.itemsJson=JSON.parse(data);
				_this.productsController.itemsReceived();
				setTimeout(hideScreenLoader,150);
			},
			fail:function(){ getProductsTry++; setTimeout(_this.getProducts,getProductsTry*1000); }
		});
	};
	var onFiltersRecive=function(){
		$('section',_this.filterElem).empty();
		$(_this.filtersJson).each(function(i,obj){
			var opts={};
			if(typeof obj.Id !='undefined') opts.id=obj.Id;
			if(typeof obj.Name !='undefined') opts.name=obj.Name;
			if(typeof obj.Text !='undefined') opts.text=obj.Text;
			if(typeof obj.Values !='undefined') opts.values=obj.Values;
			if(typeof obj.SortIndex !='undefined') opts.sortIndex=obj.SortIndex;
			if(typeof obj.SelectedValue !='undefined') opts.selectedValue=obj.SelectedValue;
			if(typeof obj.ParentId !='undefined') opts.parent=obj.ParentId;
			opts.onChange=_this.doFilter;
			opts.template=_this.filterMenuTemplate;
			
			_this.filters[i] = new filterMenu(opts);
			
			$('section',_this.filterElem).append(_this.filters[i].element);
			
		});
		if(getQSVals().length) _this.doFilter(true);
		else _this.getProducts([]);
		if(typeof _this.ready == 'function') _this.ready(_this);
		setTimeout(_this.calculateContainerHeight,100);
	};
	this.productsDataBind=function(){

	};

	this.deactiveAllFilters=function(){
		$(_this.filters).each(function(fmi, _filtermenu){
			_filtermenu.noReq=true;
			_filtermenu.hideActive();
			
			setTimeout(function(){_filtermenu.noReq=false;},200);
		});
	};


	this.doFilter=function(_fromHistory){
		var filterJson=[];
		if(!_fromHistory){
			$(_this.filters).each(function(_i,_filter){
				if(typeof _filter.activeVal!='undefined'){
					var filtername=_filter.name;
					var filtervalue=_filter.activeVal.value;
					filterJson[filterJson.length]={name:filtername,value:filtervalue};
				}
			});
			_this.getProducts(JSON.stringify(filterJson));
			_this.pushingHistory=true;
			 pushHistoryState(filterJson);
			 setTimeout(function(){_this.pushingHistory=false;},200);
		} else if(_fromHistory && !_this.pushingHistory){
			
			_this.deactiveAllFilters();
			var qs=getQSVals();
			alert(JSON.stringify(qs));
			$(qs).each(function(qsi,qsitem){
				if(typeof qsitem.name!='undefined' && typeof qsitem.value !='undefined' && qsitem.name!='' && qsitem.value!=''){

					$(_this.filters).each(function(fltri,_filter){
						var filtername=_filter.name;
						if(filtername==qsitem.name){
							$(_filter.values).each(function(vali,fval){
								if(fval.value==qsitem.value){
									_filter.noReq=true;
									_filter.valueRequesActivation(fval);
									setTimeout(function(){_filter.noReq=false;},200);
								}
							});
						}
					});
				}
			});
			_this.doFilter(false);
		}
	};
	this.calculateContainerHeight=function(){
		var filtersHeight=$(_this.filterElem).outerHeight(true);
		var mainElem=$('main');
		//$(mainElem).height($(mainElem).outerHeight(true));
		if($(mainElem).outerHeight(true) < filtersHeight+100){
			var _height=filtersHeight+100;

			//$(mainElem).height(_height).css({'min-height':_height+'px'});
			$(mainElem).css({'min-height':_height+'px'});
		}
	};
	this.onScroll=function(){
		var filterHeight=$(_this.filterelem).outerHeight(true),
			mainHeight=$('main').height();
		if(filterHeight > mainHeight){

		}
	};
	//pushHistoryState([]);
	window.onpopstate = function(event) {
		if(!_this.pushingHistory)
			_this.doFilter(true);
	};
	_this.getFilters();
	//_this.calculateContainerHeight();
	
	//onFiltersRecive();

}
function filterMenu(_opts){
	var _this=this;
	this.opts={
		id:0,
		name:'',
		text:'',
		values:[],
		sortIndex:0,
		type:1,
		selectedValue:undefined,
		parent:undefined,
		template:undefined,
		onChange:undefined
	};
	$.extend(_this.opts,_opts);
	
	this.id=_this.opts.id;
	this.name=_this.opts.name;
	this.text=_this.opts.text;
	this.values=[];
	this.sortIndex=_this.opts.sort;
	this.selectedValue=undefined;
	this.template=(typeof _this.opts.template != 'undefined') ? $(_this.opts.template).clone() : undefined;
	this.element=undefined;
	this.valueListElem=undefined;
	this.activeElemHolder=undefined;
	this.onChange=_this.opts.onChange;
	activeVal=undefined;
	this.isOpen=true;
	this.titleElem=undefined;
	this.noReq=false;
	this.open=function(){
		$(_this.valueListElem).stop().slideDown(250);
		$(_this.element).addClass('opened');
		_this.isOpen=true;
		
	};
	this.close=function(){
		$(_this.valueListElem).stop().slideUp(250);
		_this.isOpen=false;
		$(_this.element).removeClass('opened');
		
	};
	this.valueRequesActivation=function(_val){

		$(_this.values).each(function(i,obj){
			obj.deactivate();
		});
		$('.linkcontent',_this.activeElemHolder).text(_val.text);
		if(_this.type==2 && typeof _val.backgroundImage!='undefined')
			$('.icon-right',_this.activeElemHolder).css('background-image','url('+_val.backgroundImage+')');
		_val.activate();

		_this.activeVal=_val;
		_this.selectedValue=_val;

		if(typeof _this.onChange=='function' && !_this.noReq) _this.onChange(false);

		$(_this.activeElemHolder).stop().delay(200).slideDown(200);
		_this.close();

	};
	this.hideActive=function(){
		$(_this.activeElemHolder).stop().slideUp(200);
		$(_this.values).each(function(i,obj){
			obj.deactivate();
		});
		setTimeout(_this.open,150);
		_this.activeVal=undefined;
		if(typeof _this.onChange=='function' && !_this.noReq) _this.onChange(false);
	}

	//init
	if(typeof _this.template != 'undefined'){
		
		_this.activeElemHolder=$('.pfm-activeitem',_this.template).hide();
		if(_this.type==1) $('.icon-right'._this.activeElemHolder).remove();

		//Generate Values List
		_this.valueListElem=$('.pfm-items',_this.template).clone().empty();

		$(_this.activeElemHolder).click(_this.hideActive);
		var valtemp=$('.pfm-items .pmf-item:first-child',_this.template);
		$(_this.opts.values).each(function(i,obj){
			var valopts={
				text:obj.Text,
				value:obj.Value,
				backgroundImage:obj.BackImgUrl,
				onActivationRequest:_this.valueRequesActivation,
				template:valtemp
			};
			
			_this.values[i-1]= new filterMenuValue(valopts);
			
			$(_this.valueListElem).append(_this.values[i-1].element);

			
		});

		_this.element=$(_this.template).clone();
		_this.titleElem=$('.pfm-title',_this.element);
		$('.pfm-activeitem',_this.element).replaceWith(_this.activeElemHolder).hide();
		$('.pfm-items',_this.element).empty().append(_this.valueListElem);
		$('.linkcontent',_this.titleElem).text(_this.text);
		$(_this.titleElem).click(function(){
			if(_this.isOpen==true){
				_this.close();
			}
			else{
				_this.open();
			}
		});

	}
	_this.opts=undefined;
}
function filterMenuValue(_opts){
	var _this=this;
	this.opts={
		text:'',
		value:'',
		backgroundImage:undefined,
		backgroundColor:undefined,
		template:undefined,
		onActivationRequest:undefined
	};

	$.extend(_this.opts,_opts);

	this.element=undefined;
	this.text=_this.opts.text;
	this.value=_this.opts.value;
	this.backgroundImage=_this.opts.backgroundImage;
	this.backgroundColor=_this.opts.backgroundColor;
	this.onActivationRequest=_this.opts.onActivationRequest;
	this.isActive=false;

	this.deactivate=function(){
		$(_this.element).show();
		_this.isActive=false;
	};
	this.activate=function(){
		$(_this.element).hide();
		_this.isActive=true;
	};
	
	//init
	if(typeof _this.opts.template != 'undefined'){

		_this.element=$(_this.opts.template).clone();

		$('.linkcontent',_this.element).text(_this.text);
		$(_this.element).click(function(){
			if(typeof _this.onActivationRequest=='function')
				_this.onActivationRequest(_this);
		});
		
	}
	
	_this._opts=undefined;

}



































































function productsController(_opts){
	var _this=this;
	this.opts={
		itemTemplate:undefined,
		container:undefined
	};
	$.extend(_this.opts,_opts);
	this.itemsJson=[];
	this.items=[];
	this.itemTemplate=_this.opts.itemTemplate;
	this.container=_this.opts.container;
	this.iso=undefined;
	this.itemsReceived=function(){
		if(typeof _this.container != 'undefined' && typeof _this.itemTemplate != 'undefined'){
			$(_this.itemTemplate).show();
			$(_this.container).empty();
			$(_this.itemsJson).each(function(i,obj){
				obj.template=$(_this.itemTemplate).clone();
				_this.items[i]=new productItem(obj);
				$(_this.items[i].element).css('z-index',1000-i)
				$(_this.container).append(_this.items[i].element);

			});
			_this.iso=$(_this.container).isotope({
				itemSelector:'.product-item',
				layoutMode: 'masonry',
				masonry:{
					columnWidth:'.product-item'
				}
			});
			$(_this.iso).isotope( 'on', 'layoutComplete', _this.isoLayoutComplete )
			setTimeout(function(){
				$(_this.iso).isotope('layout');
			},300)
			$('.product-item .thumb-imgs').each(function(a,b){
				var isw=0;
				$('.thumb-img',b).each(function(c,d){
					isw+=$(d).outerWidth(true);
				});
				$(b).width(isw);
			});
		}
	};
	var firstIsoLayout=false;
	var firstResize=false;
	this.isoLayoutComplete=function(){
		if(!firstIsoLayout){
			firstIsoLayout=true;
			_this.onResize();
		}
	};
	
	this.onResize=function(){
		
		$(_this.items).each(function(index,item){
			item.onResize();
		});
		if(!firstResize){
			firstResize=true;
			setTimeout(function(){$(_this.iso).isotope('layout');},300)
		}
	};
	this.resizeHandler;
	$(window).resize(function(){
		clearTimeout(_this.resizeHandler);
		_this.resizeHandler=setTimeout(_this.onResize,700);
	});


	//_this.itemsReceived();

}

function productItem(_opts){
	var _this=this;
	this.opts={
		dtl1:'',
		dtl2:'',
		dtl3:'',
		dtl4:'',
		images:[],
		selectedimg:0,
		href:'#',
		available:true,
		template:undefined
	};
	$.extend(_this.opts,_opts);
	this.dtl1=_this.opts.dtl1;
	this.dtl2=_this.opts.dtl2;
	this.dtl3=_this.opts.dtl3;
	this.dtl4=_this.opts.dtl4;
	this.available=_this.opts.available;
	this.href=_this.opts.href;
	this.template=$(_this.opts.template).clone();
	this.element=(typeof _this.opts.template!='undefined') ? $(_this.opts.template).clone():undefined;
	this.images=[];
	this.activeImg=undefined;
	this.thumbsIndex=0;
	this.thumbWidth;
	
	this.thumbsBack=function(){
		var end=false,oldIndex=_this.thumbsIndex;
		if(_this.thumbsIndex>0){
			if(typeof _this.thumbWidth == 'undefined') _this.thumbWidth=$(_this.images[0].smlImgElem).outerWidth(true);
			if(typeof _this.images[_this.thumbsIndex-3] !='undefined'){
				_this.thumbsIndex-=3;
				if(typeof _this.images[_this.thumbsIndex-1] =='undefined') end=true;
			}
			else if(typeof _this.images[_this.thumbsIndex-2] !='undefined'){
				_this.thumbsIndex-=2;
				end=true;
			}
			else if(typeof _this.images[_this.thumbsIndex-1] !='undefined'){
				_this.thumbsIndex-=1;
				end=true;
			}
			if(end) $('.btn-back',_this.element).addClass('disabled');
			if(oldIndex != _this.thumbsIndex) $('.btn-forward',_this.element).removeClass('disabled');
			
			$('.thumb-imgs',_this.element).animate({'right':'-'+(_this.thumbsIndex*_this.thumbWidth)+'px'},500);
		}
	};
	this.thumbsForward=function(){
		var end=false,oldIndex=_this.thumbsIndex;

		if(_this.thumbsIndex<_this.images.length-1){
			if(typeof _this.thumbWidth == 'undefined') _this.thumbWidth=$(_this.images[0].smlImgElem).outerWidth(true);
			if(typeof _this.images[_this.thumbsIndex+5] !='undefined'){
				_this.thumbsIndex+=3;
				if(typeof _this.images[_this.thumbsIndex+1] =='undefined') end=true;
			}
			else if(typeof _this.images[_this.thumbsIndex+4] !='undefined'){
				_this.thumbsIndex+=2;
				end=true;
			}
			else if(typeof _this.images[_this.thumbsIndex+3] !='undefined'){
				_this.thumbsIndex+=1;
				end=true;
			}
			if(end) $('.btn-forward',_this.element).addClass('disabled');
			if(oldIndex!=_this.thumbsIndex) $('.btn-back',_this.element).removeClass('disabled');
			$('.thumb-imgs',_this.element).animate({'right':'-'+(_this.thumbsIndex*_this.thumbWidth)+'px'},500);
		}
	};
	this.thumbsHover=function(_img){
		_this.activeImg.hideLarge();
		_this.activeImg=_img;
		_this.activeImg.showLarge();
	};
	this.imgLoaded=function(_img){
		$(_this.images).each(function(i,img){
			if(!img.loaded && !img.loading){
				img.loadImg();
				return;
			}
		});
	};
	this.onResize=function(){
		var lrgContainer=$('.large-img-container',_this.element);
		$(lrgContainer).height($(lrgContainer).width());
	};

	$('.thumb-imgs',_this.element).empty();
	$(_this.element).mouseleave(function(){$(_this.element).removeClass('active');});
	$('.large-img-container',_this.element).empty().mouseenter(function(){$(_this.element).addClass('active');});
	$('.detail-1 .detail-content',_this.element).text(_this.dtl1);
	$('.detail-2 .detail-content',_this.element).text(_this.dtl2);
	$('.detail-3 .detail-content',_this.element).text(_this.dtl3);
	$('.detail-4 .detail-content',_this.element).text(_this.dtl4);

	$(_this.opts.images).each(function(i,obj){
		var imgopts={
			onLoadComplete:_this.imgLoaded,
			onThumbHover:_this.thumbsHover,
			lrgImgElem:$('.large-img',_this.template).clone(),
			smlImgElem:$('.thumb-img',_this.template).clone(),
			src:obj
		};
		if(i==_this.opts.selectedimg) imgopts.active=1;
		_this.images[i]=new productImage(imgopts);
		$('.thumb-imgs',_this.element).append(_this.images[i].smlImgElem);
		$('.large-img-container',_this.element).append(_this.images[i].lrgImgElem);
	});
	
	$('.btn-forward',_this.element).click(_this.thumbsForward);
	$('.btn-back',_this.element).click(_this.thumbsBack);
	_this.activeImg=_this.images[_this.opts.selectedimg];
	_this.opts=undefined;
}

function productImage(_opts){
	var _this=this;
	this.opts={
		src:'',
		lrgImgElem:undefined,
		smlImgElem:undefined,
		onLoadComplete:undefined,
		onThumbHover:undefined,
		active:0
	};
	$.extend(_this.opts,_opts);
	this.src=_this.opts.src;
	this.lrgImgElem=_this.opts.lrgImgElem;
	this.smlImgElem=_this.opts.smlImgElem;
	this.onLoadComplete=_this.opts.onLoadComplete;
	this.onThumbHover=_this.opts.onThumbHover;
	this.active=_this.opts.active;
	this.loaded=false;
	this.loading=false;
	this.holder=undefined;
	
	this.loadImg=function(){
		
		if(!_this.loaded && !_this.laoding){
			_this.laoding=true;
			_this.holder=new Image();
			$(_this.holder).load(function(){
				
				_this.loaded=true;
				if(typeof _this.onLoadComplete == 'function') _this.onLoadComplete(_this);
				_this.loading=false;

				$(_this.lrgImgElem).css('background-image','url('+_this.src+')');
				$('div',_this.smlImgElem).css('background-image','url('+_this.src+')');
				if(_this.active){ _this.showLarge(); }

			});
			_this.holder.src=_this.src;
		}
	};
	this.showLarge=function(){
		if(_this.loaded){
			_this.active=true;
			$(_this.lrgImgElem).stop().fadeIn(300);
		}
		else if(!_this.loading)
			_this.loadImg();

	};
	this.hideLarge=function(){
		_this.active=false;
		$(_this.lrgImgElem).stop().fadeOut(300);
	};

	

	if(typeof _this.onThumbHover == 'function'){
		$(_this.smlImgElem).on('mousemove',function(){
			if(!_this.loaded && !_this.loading)
				_this.loadImg();
			else if(_this.loaded && typeof _this.onThumbHover =='function' && !_this.active)
				_this.onThumbHover(_this);

		});
	}
	$(_this.lrgImgElem).hide();
	
	if(_this.active) {
		_this.loadImg();
	}

	_this.opts=undefined;
}

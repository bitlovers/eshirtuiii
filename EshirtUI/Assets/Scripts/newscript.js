
function pageControllerC(_opts){
	var _this=this;
	this.opts={
		page:''
	};
	$.extend(_this.opts,_opts);

	this.page=_this.opts.page;
	this.navController=undefined;
	this.filterController=undefined;
	this.productsController=undefined;

	this.showScreenLoader=function(e){
		if(typeof e !='undefined' && typeof e.delay !='undefined') setTimeout(_this.showScreenLoader,e.delay);
		else $('.screenLoader').addClass('active');
	};
	this.hideScreenLoader=function(e){
		if(typeof e !='undefined' && typeof e.delay !='undefined') setTimeout(_this.hideScreenLoader,e.delay);
		else $('.screenLoader').removeClass('active');
	};
	this.pushHistoryState=function(e){
		if(e.items.length){
			var pushUrl=window.location.pathname+'?';
			for (var i = 0; i < e.items.length; i++) {
				var item=e.items[i];
				pushUrl+=item.name+'='+item.value;
				if(i!=e.items.length-1) pushUrl+='&';
			};
			window.history.pushState(e.items,document.title,pushUrl);
		}
	};
	this.getQSVals=function(e){
		var vars = [], hash;
		//var q =(document.URL.split('#')[0]).split('?')[1];
	    var q = (window.location.search).split('?')[1];
	    if(q != undefined){
	        q = q.split('&');
	        for(var i = 0; i < q.length; i++){
	            hash = q[i].split('=');
	            //vars.push(hash[1]);
	            //vars[hash[0]] = hash[1];
	            vars[i]={name:hash[0],value:hash[1]};
	        }
		}
		return vars;
	}

	this.init=function(e){
		if(e.renderMaster){
			_this.navController = new navController();
		}



		if(_this.opts.page == 'products'){
			//_this.showScreenLoader();
			_this.productsController=new productsController({
		        container:$('.products'),
		        itemTemplate:$('.product-item')
		    });
			_this.filterController=new filterController({
		        getFilters:{
		            url:'/Api/Filters/GetFilters',
		            method:'post'
		        },
		        getProducts:{
		            url:'/api/products/filterproducts',
		            method:'post'
		        },
		        filterTemplate:$('.pfilter'),
		        productsController:_this.productsController,
		        pageController:_this,
		        ready:function(obj){
		            $('.pfilter').replaceWith(obj.filterElem);
		        }
		    });


		}
	};




	_this.init({
		renderMaster:false
	});
}





function filterController(_opts){
	var _this = this;
	this.opts={
		getFilters:{
			url:'/',
			data:{},
			method:'get'
		},
		getProducts:{
			url:'/',
			method:'get',
		},
		productsController:undefined,
		filterTemplate:undefined,
		container:$('<div></div>'),
		pageController:undefined
	};
	$.extend(_this.opts,_opts);

	this.productsController=_this.opts.productsController;
	this.pageController=_this.opts.pageController;
	this.filters=[];
	this.filtersJson=[];
	this.filterTemplate=$($(_this.opts.filterTemplate).clone());
	this.filterElem=$($(_this.filterTemplate).clone());
	this.filterMenuTemplate=$($('.pf-menu',_this.filterTemplate).clone());
	this.ready=_this.opts.ready;
	this.pushingHistory=false;

	var getProductsTry=0,getFiltersTry=0;


	this.getFilters=function(e){
		
		//_this.pageController.showScreenLoader({delay:100});
		$.ajax({
			url:_this.opts.getFilters.url,
			method:_this.opts.getFilters.method,
			success:function(data,stat,xhr){
					getFiltersTry=0;
					_this.filtersJson=JSON.parse(data);
					_this.filtersReceived();
			},
			fail:function(){getFiltersTry++; setTimeout(_this.getFilters,getFiltersTry*1000);}
		});
	};
	this.filtersReceived=function(e){
		
		$('section',_this.filterElem).empty();
		$(_this.filtersJson).each(function(i,obj){
			var opts={};
			if(typeof obj.Id !='undefined') opts.id=obj.Id;
			if(typeof obj.Name !='undefined') opts.name=obj.Name;
			if(typeof obj.Text !='undefined') opts.text=obj.Text;
			if(typeof obj.Values !='undefined') opts.values=obj.Values;
			if(typeof obj.SortIndex !='undefined') opts.sortIndex=obj.SortIndex;
			if(typeof obj.SelectedValue !='undefined') opts.selectedValue=obj.SelectedValue;
			if(typeof obj.ParentId !='undefined' && obj.ParentId !='' && obj.ParentId !='null' && obj.ParentId!=null) opts.parentId=obj.ParentId;
			//alert(opts.parentId);
			opts.onChange=_this.doFilter;
			opts.template=_this.filterMenuTemplate;
			_this.filters[i] = new filterMenu(opts);
			$('section',_this.filterElem).append(_this.filters[i].element);
		});
		//if(getQSVals().length) _this.doFilter(true);
		//else _this.getProducts([]);
		//_this.processActiveMenus();
		_this.handlePopState();
		if(typeof _this.ready == 'function') _this.ready(_this);
		//setTimeout(_this.calculateContainerHeight,100);






	};
	this.processActiveMenus=function(e){

		if (typeof e !='undefined' && typeof e.delay!='undefined'){
			setTimeout(_this.processActiveMenus,e.delay);
			return;
		}
		$(_this.filters).each(function(fi,filter){

			if(typeof filter.parentId !='undefined'){
				var activated=false;
				$(_this.filters).each(function(_fi,_filter){
					if(_filter.id==filter.parentId){
						if(typeof _filter.activeVal!='undefined'){
							filter.activate({delay:100});
							activated=true;
							
						}
					}
				});
				if(!activated){ filter.deactivate({delay:100}); filter.deactivateActiveValue()}
			}
		});
	};
	this.doFilter=function(e){
		pageController.showScreenLoader();
		var filterJson=[];
		$(_this.getActiveFilters()).each(function(_i,_filter){
			if(typeof _filter.activeVal!='undefined'){
				var filtername=_filter.name;
				var filtervalue=_filter.activeVal.value;
				filterJson[filterJson.length]={name:filtername,value:filtervalue};
			}
		});
		_this.getProducts({filtersString:JSON.stringify(filterJson)});
		//_this.pushingHistory=true;
		if(typeof e == 'undefined' || typeof e.noPush == 'undefined' || e.noPush == false){
			_this.pageController.pushHistoryState({items:filterJson});

		}
		/*if(typeof e !='undefined' && typeof e.filterMenu !='undefined'){
			$(_this.filters).each(function(fi,f){
				if(f.parentId==e.filterMenu.id){
					f.activate();
				}
			});
		}*/
		_this.processActiveMenus({delay:100});
	};

	this.getActiveFilters=function(e){
		var actives=[];
		$(_this.filters).each(function(i,filter){
			if(typeof filter.activeVal != 'undefined')
				actives[actives.length]=filter;
		});
		return actives;
	};
	this.activateFilterByNameValue=function(e){
		$(_this.filters).each(function(fltri,filter){
			if(filter.name==e.name){
				$(filter.values).each(function(vali,value){
					if(value.value==e.value){
						filter.valueActivationRequest({filterValue:value,noReq:true});
					}
				});
			}
		});
	};

	/*#################################################*/
	/*#################################################*/
	/*#################################################*/
	/*#################################################*/



	this.getProducts=function(e){
		//alert(e.filtersString);
		$.ajax({
			url:_this.opts.getProducts.url,
			method:_this.opts.getProducts.method,
			data:e.filtersString,
			success:function(data,stat,xhr){
				getProductsTry=0;
				_this.productsController.itemsJson=JSON.parse(data);
				_this.productsController.dataBind();
				
			},
			fail:function(){ getProductsTry++; setTimeout(_this.getProducts,getProductsTry*1000); }
		});
	};

	this.productsReceived=function(e){

	};









	this.handlePopState=function(e){
		$(_this.filters).each(function(fltri,filter){
			filter.deactivateActiveValue({noReq:true});
		});
		$(_this.pageController.getQSVals()).each(function(qsi,qsitem){
			_this.activateFilterByNameValue(qsitem);
		});
		//_this.processActiveMenus();



		_this.doFilter({noPush:true});
	};





	this.init=function(e){

		_this.getFilters();

		window.onpopstate = _this.handlePopState;

	};


	_this.init();




}
function filterMenu(_opts){
	var _this=this;
	this.opts={
		id:0,
		name:'',
		text:'',
		values:[],
		sortIndex:0,
		type:1,
		selectedValue:undefined,
		parentId:undefined,
		template:undefined,
		onChange:undefined
	};
	$.extend(_this.opts,_opts);

	this.id=_this.opts.id;
	this.name=_this.opts.name;
	this.text=_this.opts.text;
	this.values=[];
	this.sortIndex=_this.opts.sort;
	this.selectedValue=undefined;
	this.parentId=_this.opts.parentId;
	this.template=(typeof _this.opts.template != 'undefined') ? $(_this.opts.template).clone() : undefined;
	this.element=undefined;
	this.valueListElem=undefined;
	this.activeElemHolder=undefined;
	this.onChange=_this.opts.onChange;
	this.activeVal=undefined;
	this.isOpen=true;
	this.titleElem=undefined;
	_this.isActive=true;
	this.activate=function(e){
		if(typeof e !='undefined' && typeof e.delay!=undefined) setTimeout(_this.activate,e.delay);
		else{
			_this.isActive=true;
			$(_this.element).stop().slideDown(250).css('height','auto');
		}
	};
	this.deactivate=function(e){
		//alert('deactivate');
		
		if(typeof e !='undefined' && typeof e.delay!=undefined) setTimeout(_this.deactivate,e.delay);
		else { $(_this.element).stop().slideUp(200); _this.isActive=false; }
	};
	this.open=function(e){
		$(_this.element).addClass('opened');
		_this.isOpen=true;
		$(_this.valueListElem).stop().slideDown(250);
	};
	this.close=function(e){
		$(_this.element).removeClass('opened');
		_this.isOpen=false;
		$(_this.valueListElem).stop().slideUp(250);
	};
	this.activateValue=function(e){
		if(typeof e.filterValue !=undefined && _this.activeVal != e.filterValue){
			if(typeof _this.activeVal !='undefined')
				_this.activeVal.deactivate();
			_this.activeVal=e.filterValue;
			$('.linkcontent',_this.activeElemHolder).text(_this.activeVal.text);
			if(_this.type==2 && typeof _this.activeVal.backgroundImage!='undefined')
				$('.icon-right',_this.activeElemHolder).css('background-image','url('+_val.backgroundImage+')');
			_this.activeVal.activate();
			$(_this.activeElemHolder).stop().delay(200).slideDown(200);
			
			if(typeof e.noReq =='undefined' || e.noReq == false){
				_this.onChange({filterMenu:_this});
			}
			setTimeout(_this.close,10);
			//_this.close();
		}
	};
	this.deactivateActiveValue=function(e){
		if(typeof _this.activeVal!='undefined'){
			$(_this.activeElemHolder).stop().slideUp(200);
			_this.activeVal.deactivate();
			_this.activeVal=undefined;
			_this.open();
			if(typeof e.noReq =='undefined' || e.noReq == false)
				_this.onChange({filterMenu:_this});
		}
	};
	this.valueActivationRequest=function(e){
		_this.activateValue(e);
	};


	this.init=function(e){
		
		if(typeof _this.template != 'undefined'){
			_this.activeElemHolder=$('.pfm-activeitem',_this.template).hide();
			if(_this.type==1) $('.icon-right'._this.activeElemHolder).remove();

			//Generate Values List
			_this.valueListElem=$('.pfm-items',_this.template).clone().empty();

			$(_this.activeElemHolder).click(_this.deactivateActiveValue);
			var valtemp=$('.pfm-items .pmf-item:first-child',_this.template);
			$(_this.opts.values).each(function(i,obj){
				var valopts={
					text:obj.Text,
					value:obj.Value,
					backgroundImage:obj.BackImgUrl,
					onActivationRequest:_this.valueActivationRequest,
					template:valtemp
				};
				
				_this.values[i]= new filterMenuValue(valopts);
				
				$(_this.valueListElem).append(_this.values[i].element);

				
			});

			_this.element=$(_this.template).clone();
			if(typeof _this.parentId != 'undefined') { _this.deactivate({delay:200});}
			_this.titleElem=$('.pfm-title',_this.element);
			$('.pfm-activeitem',_this.element).replaceWith(_this.activeElemHolder).hide();
			$('.pfm-items',_this.element).empty().append(_this.valueListElem);
			$('.linkcontent',_this.titleElem).text(_this.text);
			$(_this.titleElem).click(function(){
				if(_this.isOpen==true){
					_this.close();
				}
				else{
					_this.open();
				}
			});

		}

	};

	_this.init();
}
function filterMenuValue(_opts){
	var _this=this;
	this.opts={
		text:'',
		value:'',
		backgroundImage:undefined,
		backgroundColor:undefined,
		template:undefined,
		onActivationRequest:undefined
	};

	$.extend(_this.opts,_opts);

	this.element=undefined;
	this.text=_this.opts.text;
	this.value=_this.opts.value;
	this.backgroundImage=_this.opts.backgroundImage;
	this.backgroundColor=_this.opts.backgroundColor;
	this.onActivationRequest=_this.opts.onActivationRequest;
	this.isActive=false;


	this.activate=function(e){
		$(_this.element).hide();
		_this.isActive=true;
	};
	this.deactivate=function(e){
		$(_this.element).show();
		_this.isActive=false;
	};
	this.init=function(e){
		if(typeof _this.opts.template != 'undefined'){

			_this.element=$(_this.opts.template).clone();

			$('.linkcontent',_this.element).text(_this.text);
			$(_this.element).click(function(){
				if(typeof _this.onActivationRequest=='function')
					_this.onActivationRequest({filterValue:_this});
			});
		}
	};


	_this.init();


}






















































function productsController(_opts){
	var _this=this;
	this.opts={
		itemTemplate:undefined,
		container:undefined
	};
	$.extend(_this.opts,_opts);

	this.itemsJson=[];
	this.items=[];
	this.itemTemplate=_this.opts.itemTemplate;
	this.container=_this.opts.container;
	this.iso=undefined;
	this.activeElems=undefined;
	this.dataBind=function(e){

		if(typeof _this.container != 'undefined' && typeof _this.itemTemplate != 'undefined'){
			$(_this.itemTemplate).show();
			//$(_this.container).empty();
			$(_this.itemsJson).each(function(i,obj){
				obj.template=$(_this.itemTemplate).clone();
				_this.items[i]=new productItem(obj);
				$(_this.items[i].element).css('z-index',1000-i)
				//$(_this.container).append(_this.items[i].element);

			});
			if(_this.activeElems!=undefined && _this.activeElems.length){
				$(_this.iso).isotope('remove',_this.activeElems);
				_this.activeElems=undefined;
			}
			
			//_this.activeElems
			var a=$('<div></div>');
			
			$(_this.items).each(function(ii,item){
				//_this.activeElems[_this.activeElems.length]=item.element;
				$(a).append($(item.element));
				
			});
			//alert($(a).html());
			_this.activeElems=$(a).children();
			
			//$(_this.container);
			$(_this.container).isotope('insert',$(_this.activeElems));
			//$(_this.container).isotope('layout');
			//alert($(_this.activeElems[0]).html());
			/*
			_this.iso=$(_this.container).isotope({
				itemSelector:'.product-item',
				layoutMode: 'masonry',
				masonry:{
					columnWidth:'.product-item'
				}
			});
			$(_this.iso).isotope( 'on', 'layoutComplete', _this.isoLayoutComplete )
			setTimeout(function(){
				$(_this.iso).isotope('layout');
			},300)
			$('.product-item .thumb-imgs').each(function(a,b){
				var isw=0;
				$('.thumb-img',b).each(function(c,d){
					isw+=$(d).outerWidth(true);
				});
				$(b).width(isw);
			});
			*/
			pageController.hideScreenLoader();
		}

		









	};

	this.onWindowResize=function(e){

	};
	_this.init=function(e){
		$(_this.container).empty();
		_this.iso=$(_this.container).isotope({
			itemSelector:'.product-item',
			layoutMode: 'masonry',
			masonry:{
				columnWidth:'.product-item'
			},
			isInitLayout: false,
			isOriginLeft: false,
			percentPosition: true,
		});
		
	};

	_this.init();

}

function productItem(_opts){
	var _this=this;
	this.opts={
		dtl1:'',
		dtl2:'',
		dtl3:'',
		dtl4:'',
		images:[],
		selectedimg:0,
		href:'#',
		available:true,
		template:undefined
	};
	$.extend(_this.opts,_opts);
	this.dtl1=_this.opts.dtl1;
	this.dtl2=_this.opts.dtl2;
	this.dtl3=_this.opts.dtl3;
	this.dtl4=_this.opts.dtl4;
	this.available=_this.opts.available;
	this.href=_this.opts.href;
	this.template=$(_this.opts.template).clone();
	this.element=(typeof _this.opts.template!='undefined') ? $(_this.opts.template).clone():undefined;
	
	this.images=[];
	this.activeImg=undefined;
	this.thumbsIndex=0;
	this.thumbWidth;

	this.thumbsBack=function(e){

		var end=false,oldIndex=_this.thumbsIndex;
		if(_this.thumbsIndex>0){
			if(typeof _this.thumbWidth == 'undefined') _this.thumbWidth=$(_this.images[0].smlImgElem).outerWidth(true);
			if(typeof _this.images[_this.thumbsIndex-3] !='undefined'){
				_this.thumbsIndex-=3;
				if(typeof _this.images[_this.thumbsIndex-1] =='undefined') end=true;
			}
			else if(typeof _this.images[_this.thumbsIndex-2] !='undefined'){
				_this.thumbsIndex-=2;
				end=true;
			}
			else if(typeof _this.images[_this.thumbsIndex-1] !='undefined'){
				_this.thumbsIndex-=1;
				end=true;
			}
			if(end) $('.btn-back',_this.element).addClass('disabled');
			if(oldIndex != _this.thumbsIndex) $('.btn-forward',_this.element).removeClass('disabled');
			
			$('.thumb-imgs',_this.element).animate({'right':'-'+(_this.thumbsIndex*_this.thumbWidth)+'px'},500);
		}


	};
	this.thumbsForward=function(e){
		var end=false,oldIndex=_this.thumbsIndex;

		if(_this.thumbsIndex<_this.images.length-1){
			if(typeof _this.thumbWidth == 'undefined') _this.thumbWidth=$(_this.images[0].smlImgElem).outerWidth(true);
			if(typeof _this.images[_this.thumbsIndex+5] !='undefined'){
				_this.thumbsIndex+=3;
				if(typeof _this.images[_this.thumbsIndex+1] =='undefined') end=true;
			}
			else if(typeof _this.images[_this.thumbsIndex+4] !='undefined'){
				_this.thumbsIndex+=2;
				end=true;
			}
			else if(typeof _this.images[_this.thumbsIndex+3] !='undefined'){
				_this.thumbsIndex+=1;
				end=true;
			}
			if(end) $('.btn-forward',_this.element).addClass('disabled');
			if(oldIndex!=_this.thumbsIndex) $('.btn-back',_this.element).removeClass('disabled');
			$('.thumb-imgs',_this.element).animate({'right':'-'+(_this.thumbsIndex*_this.thumbWidth)+'px'},500);
		}
	};
	this.thumbsHover=function(e){
		_this.activeImg.hideLarge();
		_this.activeImg=e.img;
		_this.activeImg.showLarge();
	};
	this.imgLoaded=function(e){
		$(_this.images).each(function(i,img){
			if(!img.loaded && !img.loading){
				img.loadImg();
				return;
			}
		});
	};

	this.init=function(e){
		$('.thumb-imgs',_this.element).empty();
		$(_this.element).mouseleave(function(){$(_this.element).removeClass('active');});
		$('.large-img-container',_this.element).empty().mouseenter(function(){$(_this.element).addClass('active');});
		$('.detail-1 .detail-content',_this.element).text(_this.dtl1);
		$('.detail-2 .detail-content',_this.element).text(_this.dtl2);
		$('.detail-3 .detail-content',_this.element).text(_this.dtl3);
		$('.detail-4 .detail-content',_this.element).text(_this.dtl4);
		$('.details-container',_this.element).attr('href',_this.href);
		$(_this.opts.images).each(function(i,obj){
			var imgopts={
				onLoadComplete:_this.imgLoaded,
				onThumbHover:_this.thumbsHover,
				lrgImgElem:$('.large-img',_this.template).clone(),
				smlImgElem:$('.thumb-img',_this.template).clone(),
				src:obj
			};
			if(i==_this.opts.selectedimg) imgopts.active=1;
			_this.images[i]=new productImage(imgopts);
			$('.thumb-imgs',_this.element).append(_this.images[i].smlImgElem);
			$('.large-img-container',_this.element).append(_this.images[i].lrgImgElem);
		});
		
		$('.btn-forward',_this.element).click(_this.thumbsForward);
		$('.btn-back',_this.element).click(_this.thumbsBack);
		_this.activeImg=_this.images[_this.opts.selectedimg];
		_this.opts=undefined;

	};


	_this.init();
}

function productImage(_opts){
	var _this=this;
	this.opts={
		src:'',
		lrgImgElem:undefined,
		smlImgElem:undefined,
		onLoadComplete:undefined,
		onThumbHover:undefined,
		active:0
	};
	$.extend(_this.opts,_opts);
	this.src=_this.opts.src;
	this.lrgImgElem=_this.opts.lrgImgElem;
	this.smlImgElem=_this.opts.smlImgElem;
	this.onLoadComplete=_this.opts.onLoadComplete;
	this.onThumbHover=_this.opts.onThumbHover;
	this.active=_this.opts.active;
	this.loaded=false;
	this.loading=false;
	this.holder=undefined;
	
	this.loadImg=function(){
		
		if(!_this.loaded && !_this.laoding){
			_this.laoding=true;
			_this.holder=new Image();
			$(_this.holder).load(function(){
				
				_this.loaded=true;
				if(typeof _this.onLoadComplete == 'function') _this.onLoadComplete(_this);
				_this.loading=false;

				$(_this.lrgImgElem).css('background-image','url('+_this.src+')');
				$('div',_this.smlImgElem).css('background-image','url('+_this.src+')');
				if(_this.active){ _this.showLarge(); }

			});
			_this.holder.src=_this.src;
		}
	};
	this.showLarge=function(){
		if(_this.loaded){
			_this.active=true;
			$(_this.lrgImgElem).stop().fadeIn(300);
		}
		else if(!_this.loading)
			_this.loadImg();

	};
	this.hideLarge=function(){
		_this.active=false;
		$(_this.lrgImgElem).stop().fadeOut(300);
	};

	

	if(typeof _this.onThumbHover == 'function'){
		$(_this.smlImgElem).on('mousemove',function(){
			if(!_this.loaded && !_this.loading)
				_this.loadImg();
			else if(_this.loaded && typeof _this.onThumbHover =='function' && !_this.active)
				_this.onThumbHover({img:_this});

		});
	}
	$(_this.lrgImgElem).hide();
	
	if(_this.active) {
		_this.loadImg();
	}

	_this.opts=undefined;
}
